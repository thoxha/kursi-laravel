<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('home');

})->name('home');

Route::get('contact', function() {
    return view('shop.contact');
});



Route::post('contact', "PageController@send");

Route::get('news/{id}', "PageController@news")->name('news');

Route::get('news', "PageController@index");


Route::get('profile', function() {
    return "Ky eshte profili yt";
})->name('profile');

Route::get('login', function() {
    echo "<form method='post' action='".route('loginpost')."'> 
<input type='hidden' name='_token' value='".csrf_token()."'>
    <input type='text' name='username'>
    <input type='text' name='password'>
    <input type='submit' value='LOGIN'>
";

})->name('loginget');


Route::post('login', function() {
    if ($_POST['username'] == "ick" && $_POST['password'] =="1234") {
        return redirect()->route('profile');
    } else {
        return redirect()->route('loginget');
    }

} )->name('loginpost');

Route::get('dalja', function() {
    //
    //
    //
    return redirect()->route('home');
});

/*Route::namespace('Admin')->group(function () {
    Route::get('index', function() {
        return "Admin index";
    });
});*/


Route::domain('{account}.portali.test')->group(function () {
    Route::get('user/{id}', function ($account, $id) {
       // abc.portali.test/user/5
    });
});


Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        return "Admin/Users";
    });
    Route::get('profile', function () {
        return "Admin/Profile";
    });
});


Route::name('admin.')->group(function () {
    Route::get('users', function () {
        return "Gjendeni ne admin/users";
    })->name('users');
});


Route::get('showpost/{post}', "PageController@showPost");

Route::get('show/abc/{id}/{lang?}', 'PageController@show');


Route::get('lexo', "PageController@lexo");
Route::get('ndrysho', "PageController@ndrysho");

Route::get('add_category/{name}', "PageController@addCategory")->name('addCategory');


Route::get('update_category_form/{id}', "PageController@updateCategoryForm")->name('updateCategoryForm');
Route::post('update_category', "PageController@updateCategory")->name('updateCategory');
Route::get('delete_category/{id}', "PageController@deleteCategory")->name('deleteCategory');
Route::get('categories', "PageController@categories")->name('categories');
Route::get('chapter', "PageController@chapter");

Route::get('posts_by_author/{id}', "PageController@postsByAuthor")->name('postsByAuthor');
Route::get('posts_by_category/{category}', "PageController@postsByCategory")->name('postsByCategory');
Route::get('mass', "PageController@mass")->name('mass');
Route::get('product', "PageController@product");


Route::fallback(function () {
    return "E ke humb rrugen";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
