<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public $timestamps = false;

    public function category() {
        return $this->belongsTo('\App\Category' );
    }

    public function author() {
        return $this->belongsTo('\App\Author');
    }

    public function comments() {
        return $this->hasMany('\App\Comment');
    }
}
