<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    public $timestamps = false;


    public function getPriceWithVatAttribute() {
        return $this->attributes['price'] * 1.18;
    }

    public function getTotalAttribute() {
        return $this->attributes['price'] * $this->attributes['quantity'];
    }

    public function getTotalWithCurrencyAttribute() {
        return number_format($this->attributes['price'] * $this->attributes['quantity'], 2)." Euro";
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = Str::slug($name);
    }
}
