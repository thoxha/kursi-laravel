<?php

namespace App\Http\Controllers;

use App\Author;
use App\Category;
use App\Chapter;
use App\Comment;
use App\Post;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class PageController extends Controller
{
    public function send(Request $request)
    {
        echo $request->input('emri');
        echo "<br>";
        echo $request->input('emaili');
        echo "<br>";
        echo $request->input('password');
    }

    public function index()
    {

        return view('index');
        $news = [
            1 => "Partitë nuk tregojnë shpenzimet për fushatë zgjedhore",
            2 => "Kundërshtari i Furyt, Otto Wallin ishte ndeshur dy herë me Joshuan në boksin amator",
            3 => "Hoferer: Pavarësia e Kosovës fakt historik dhe realitet i pakthyeshëm"
        ];

        foreach ($news as $id => $title) {
            echo "<p><a href='" . route('news', [$id]) . "'>$title</a></p>";
        }
    }

    public function postsByAuthor($id)
    {
//        $author = Author::find($id);
//        foreach($author->posts as $post) {
//            echo "<h2>".$post->title."</h2>";
//            echo "<div>".$post->content."</div>";
//       }

        foreach (Author::find($id)->comments as $comment) {
            echo "<p>" . $comment->comment . "</p>";
        }

    }

    public function postsByCategory(Category $category)
    {
        $data['category'] = $category;
        $data['posts'] = $category->posts;
        return view('posts_by_category', $data);
    }

    public function news($id)
    {
//        $post = Post::join('categories', 'categories.id' ,  '=', 'posts.category_id')
//            ->where('posts.id', $id)->first();

        $post = Post::find($id);

        echo "<div><h1>" . $post->title . "</h1></div>";
        echo "<div>" . $post->content . "</div>";
        echo "<div>" . $post->category->id . "</div>";
        echo "<div>" . $post->category->category_name . "</div>";
        echo "<div>" . $post->author->author_name . "</div>";
        foreach ($post->comments as $comment) {
            echo "<p>Komenti #: " . $comment->id . "</p>";
            echo "<p>Autori #: " . $comment->user_id . "</p>";
            echo "<p>Autori #: " . $comment->user->name . "</p>";
            echo "<div>" . $comment->comment . "</div>";
        }
    }

    public function showPost(Post $post)
    {
        echo "<h1>" . $post->title . "</h1>";
        echo "<div>" . $post->content . "</div>";
    }

    public function show($id, $lang = "en")
    {
        echo "Gjuha: " . $lang;
        if ($id == 0) {
            echo "<p>Lista  e lajmeve";
        } else {
            echo "<p>Lajmi nr. " . $id;
        }
    }

    public function ndrysho()
    {
        $p = Post::where('p_id', 1)->first();
        $p->title = "Titulli i ndryshuar";
        $p->save();
    }


    public function lexo()
    {
        $p = Post::where('p_id', '>', 1)
            ->orderBy('title', 'desc')
            ->get();

        foreach ($p as $post) {
            echo "<p>" . $post->title . "</p>";
        }

        die();
        //  first() --> object  $p->title
        // get()  --> collection of object $p[0]->title

        if ($p != null) {
            echo $p->title;
        } else {
            echo "Nuk e gjeta";
        }


    }


    public function addCategory($name)
    {
        // INSERT
        $c = new Category;
        $c->category_name = $name;
        $c->save();
    }

    public function updateCategoryForm($id)
    {
        $category = Category::find($id);

        return view('update_category_form', ['category' => $category]);

    }


    public function updateCategory(Request $request)
    {
        // MASS UPDATE
        $c = Category::find($request->input('cat_id'));
        if ($c != null) {
            $lista = collect($request->all())
                ->except('_token')
                ->toArray();

            $c->update($lista);
        } else {
            echo "Nuk ekziston kjo kategori";
        }
    }

    public function categories()
    {
        $category = Category::where('cat_id', 118)->toSql();

        echo($category);


        die();
        $categories = Category::where('cat_id', '>', 77)->get();
        if ($categories->count() > 0) {
            foreach ($categories as $category) {
                echo "<p>" . $category->category_name . "</p>";
            }
        } else {
            echo "Nuk ka rezultate";
        }


        echo "<p>-----------------</p>";
        $categories = Category::withTrashed()->get();
        foreach ($categories as $category) {
            $css = '';
            if ($category->trashed()) {
                $css = " style='color:red' ";
            }

            echo "<p $css>" . $category->category_name . "</p>";

        }
    }

    public function deleteCategory($id)
    {
        $category = Category::find($id);
        if ($category != null) {
            $category->delete();
        }
    }


    public function mass()
    {

        $c = Category::find(1);

        $lista = [
            'cat_id' => 1,
            'category_name' => "Prova",
            'category_date' => 777,
            'parent_id' => 100,
            'is_admin' => 1
        ];


        $c->update($lista);
    }

    public function chapter()
    {

        $chapter = Chapter::find(1);
        var_dump($chapter);

    }

    public function product() {

//        if (Auth::user()->is_admin == true) {
//            echo "Ti je admin";
//        } else {
//            echo "Ti nuk je admin";
//        }
//
//        die();
//        $product = Product::find(1);
//        echo "<p>Vlera:".$product->total;
//        echo "<p>Vlera:".$product->total_with_currency;
//        echo "<p>Slug:".$product->slug;

        $product = new Product;
        $product->name = "Blla blla blla";
        $product->quantity = 6;
        $product->price = 2.45;
        $product->save();
    }



}
