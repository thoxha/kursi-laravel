<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function send(Request $request)
    {
        echo $request->input('emri');
        echo "<br>";
        echo $request->input('emaili');
        echo "<br>";
        echo $request->input('password');
    }

    public function index()
    {
        $news = [
            1 => "Partitë nuk tregojnë shpenzimet për fushatë zgjedhore",
            2 => "Kundërshtari i Furyt, Otto Wallin ishte ndeshur dy herë me Joshuan në boksin amator",
            3 => "Hoferer: Pavarësia e Kosovës fakt historik dhe realitet i pakthyeshëm"
        ];

        foreach ($news as $id => $title) {
            echo "<p><a href='" . route('news', [$id]) . "'>$title</a></p>";
        }
    }

    public function news($id)
    {
//        $post = Post::find($id);
//        $post = Post::where('id', $id)->first();

        $post = Post::findOrFail($id);
        echo "<div><h1>" . $post->title . "</h1></div>";
        echo "<div>" . $post->content . "</div>";
    }
}
