<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
//    use SoftDeletes;

    public $timestamps = false;
    protected $fillable = ['id', 'category_name', 'category_date'];
    protected $guarded = ['parent_id', 'is_admin'];


    public function posts() {
        return $this->hasMany('\App\Post');
    }
}
